import {Component, OnInit} from '@angular/core';
import {AvailableComponents, ComponentStateDescription} from './available-components';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements  OnInit {

  localStates: Array<ComponentStateDescription> = [];

  ngOnInit(): void {
    this.localStates = AvailableComponents;
  }


}
