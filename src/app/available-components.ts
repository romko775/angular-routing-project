export interface ComponentStateDescription {
  state: string;
  name: string;
  title: string;
}

export const AvailableComponents: Array<ComponentStateDescription> = [
  {
    state: 'home',
    name: 'home',
    title: 'Home'
  },
  {
    state: 'about',
    name: 'about',
    title: 'About'
  }
];
