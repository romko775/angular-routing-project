import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import {UIRouterModule} from '@uirouter/angular';
import {allStates} from './states';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    UIRouterModule.forRoot({
      states: allStates
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
