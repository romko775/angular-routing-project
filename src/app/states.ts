import {Ng2StateDeclaration} from '@uirouter/angular';
import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';


export const homeState: Ng2StateDeclaration = {
  name: 'home',
  component: HomeComponent,
  url: '/home'
};

export const aboutState: Ng2StateDeclaration = {
  name: 'about',
  component: AboutComponent,
  url: '/about'
};

export const allStates: Array<Ng2StateDeclaration> = [
  homeState,
  aboutState
];

